@extends('layouts.app')

@section('content')
<h3>Novas Férias:</h3>
<div class="card">
    <div class="card-body">
        <form action="/ferias01" method="post">
        <label for="nome">Nome:</label>
            <input type="text" id="nome" name="nome" placeholder="Nome do Funcionário" class="form-control">
        <br>
        <label for="setor"> Setor:</label>
            <input type="text" id="setor " name="setor" placeholder="Nome do Setor" class="form-control">
        <br>

        <div class="row">
            <div class="col">
                <label for="data_inicio">Início Férias:</label>
                <input type="date"name="data_inicio" class="form-control" placeholder="Data início">
                <br>

            </div>
            <div class="col">
                <label for="data_fim">Término Férias:</label>
                <input type="date" class="form-control" name="data_fim" placeholder="Data fim">
            </div>
        </div>

        <div class="form-group">
            <label for="exampleFormControlTextarea1">Observação:</label>
            <textarea class="form-control"name="obs" id="obs" rows="3"></textarea>
        </div>

        <br>
            <button class='btn btn-success' type="submit">Criar Novo cadastro</button>
        </form>
    </div>
</div>
@endsection
